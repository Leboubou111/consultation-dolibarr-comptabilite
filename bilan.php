<?php 
include('include/include.php') ;
//variable
$total_debit=0; 
$total_credit=0;
$total_partie=0 ;
$titre=0 ; // flag pour l'affichage des titres
echo '<h1>' . $titre_du_site . '</h1>' 
        . '<h3>Du ' .  $_SESSION['periode'][0]->format('j/m/Y') .' Au '  .  $_SESSION['periode'][1]->format('j/m/Y') . '</h3>' ;  
echo "<h2>Bilan</h2><h3>Au " . $_SESSION['periode'][1]->format('j/m/Y') . "</h3><br>" ; //titre et affichage de la date du bilan
//tableau de gauche
echo "<div id=\"global\"><div id=\"gauche\"><h2> Actifs </h2>" ;

//affichage de l'actif immobilisé
$listecompte=lister_compte($bdd,$_SESSION['periode'],$preg="^[2]") ;
echo '<table border=4 cellpading=50 align=center>' ; // entete tableau
foreach($listecompte as $compte)
    {
    $parametre=parametre_compte($bdd,$_SESSION['periode'],$compte,0) ;
    if($titre==0) {echo '<h3>Actif immobilisé</h3>' ; $titre=1 ;} //affichage du titre si pas encore affiche
    if ($parametre['soldecredit']) echo '<tr align=center ><td>' . $compte . '</td><td>' . $parametre['label'] . '</td><td>' . $parametre['soldecredit'] . " €</td></tr>";
    $total_debit += $parametre['soldecredit'] ;
    $total_partie += $parametre['soldecredit'] ;
    }

echo "</table>"  ;  
if($total_partie!=0) {echo "<h4>Total Actif immobilisé : " . $total_partie . " €</h4>" ; $total_partie=0 ; } // on affciche le total s'il n'est pas nul
$titre=0 ;

//affichage des stocks
$listecompte=lister_compte($bdd,$_SESSION['periode'],$preg="^[3]") ;
echo '<table border=4 cellpading=50 align=center>' ; // entete tableau
foreach($listecompte as $compte)
    {
    $parametre=parametre_compte($bdd,$_SESSION['periode'],$compte,0) ;
    if($titre==0) {echo '<h3>Stocks</h3>' ; $titre=1 ;} //affichage du titre si pas encore affiche
    if ($parametre['soldecredit']) echo '<tr align=center ><td>' . $compte . '</td><td>' . $parametre['label'] . '</td><td>' . $parametre['soldecredit'] . " €</td></tr>";
    $total_debit += $parametre['soldecredit'] ;
    $total_partie += $parametre['soldecredit'] ;
    }

echo "</table>"  ;  
if($total_partie!=0) {echo "<h4>Total Stocks : " . $total_partie . " €</h4>" ; $total_partie=0 ; } // on affciche le total s'il n'est pas nul
$titre=0 ;

//affichage de créances
echo '<table border=4 cellpading=50 align=center>' ; // entete tableau
foreach($_SESSION['bilan_conf'] as $compte)
    {
    $parametre=parametre_compte($bdd,$_SESSION['periode'],$compte,0) ;
    if($titre==0) {echo '<h3>Créances</h3>' ; $titre=1 ;} //affichage du titre si pas encore affiche
    if ($parametre['soldecredit']) echo '<tr align=center ><td>' . $compte . '</td><td>' . $parametre['label'] . '</td><td>' . $parametre['soldecredit'] . " €</td></tr>";
    $total_debit += $parametre['soldecredit'] ;
    $total_partie += $parametre['soldecredit'] ;
    }

echo "</table>"  ;  
if($total_partie!=0) {echo "<h4>Total Créances : " . $total_partie . " €</h4>" ; $total_partie=0 ; } // on affciche le total s'il n'est pas nul
$titre=0 ;

//affichage des disponibilités
$listecompte=lister_compte($bdd,$_SESSION['periode'],$preg="^[5]") ;
echo '<table border=4 cellpading=50 align=center>' ; // entete tableau
foreach($listecompte as $compte)
    {
    $parametre=parametre_compte($bdd,$_SESSION['periode'],$compte,0) ;
    if($titre==0) {echo '<h3>Disponibilités</h3>' ; $titre=1 ;} //affichage du titre si pas encore affiche
    if ($parametre['soldecredit']) echo '<tr align=center ><td>' . $compte . '</td><td>' . $parametre['label'] . '</td><td>' . $parametre['soldecredit'] . " €</td></tr>";
    $total_debit += $parametre['soldecredit'] ;
    $total_partie += $parametre['soldecredit'] ;
    }

echo "</table>"  ;  
if($total_partie!=0) {echo "<h4>Total Disponibilités : " . $total_partie . " €</h4>" ; $total_partie=0 ; } // on affciche le total s'il n'est pas nul
$titre=0 ;

//AFFICHAGE DU TOTAL DE L'ACTIF
echo "<h3>Total actif: " . $total_debit . " €</h3></div>" ; 
//Tableau du passif
echo "<div id=\"droite\"><h2>Passif</h2><table border=4 cellpading=50 align=center>" ;

//affichage du capital
$listecompte=lister_compte($bdd,$_SESSION['periode'],$preg="^[1]") ;
echo '<table border=4 cellpading=50 align=center>' ; // entete tableau
foreach($listecompte as $compte)
    {
    $parametre=parametre_compte($bdd,$_SESSION['periode'],$compte,0) ;
    if($titre==0) {echo '<h3>Fonds Propres</h3>' ; $titre=1 ;} //affichage du titre si pas encore affiche
    if ($parametre['soldedebit']) echo '<tr align=center ><td>' . $compte . '</td><td>' . $parametre['label'] . '</td><td>' . $parametre['soldedebit'] . " €</td></tr>";
    $total_credit += $parametre['soldedebit'] ;
    $total_partie += $parametre['soldedebit'] ;
    }

echo "</table>"  ;  
if($total_partie!=0) {echo "<h4>Total Fonds propres : " . $total_partie . " €</h4>" ; $total_partie=0 ; } // on affciche le total s'il n'est pas nul
$titre=0 ;

//affichage des dettes
$listecompte=lister_compte($bdd,$_SESSION['periode'],$preg="^[4]") ;
echo '<table border=4 cellpading=50 align=center>' ; // entete tableau
foreach($listecompte as $compte)
    {
    if (!in_array($compte , $_SESSION['bilan_conf'] ))
        {
        $parametre=parametre_compte($bdd,$_SESSION['periode'],$compte,0) ;
        if($titre==0) {echo '<h3>Dettes</h3>' ; $titre=1 ;} //affichage du titre si pas encore affiche
        if ($parametre['soldedebit']) echo '<tr align=center ><td>' . $compte . '</td><td>' . $parametre['label'] . '</td><td>' . $parametre['soldedebit'] . " €</td></tr>";
        $total_credit += $parametre['soldedebit'] ;
        $total_partie += $parametre['soldedebit'] ;
        }
    }
echo "</table>"  ;  
if($total_partie!=0) {echo "<h4>Total Dettes : " . $total_partie . " €</h4>" ; $total_partie=0 ; } // on affciche le total s'il n'est pas nul
$titre=0 ;

//Calcul du résultat
$resultat=round(-$total_credit+$total_debit,2) ;
if ($resultat < 0) { echo "<h3>Résultat (déficit): " . $resultat . " €</h3>" ; }
elseif ($resultat > 0) { echo "<h3>Résultat (bénéfices): " . $resultat . " €</h3>" ; }
$total_credit+=$resultat ;

//AFFICHAGE DU TOTAL DE L'ACTIF
echo "<h3>Total passif: " . $total_credit . " €</h3></div>" ; 
echo "</body></html>" ;
