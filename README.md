Cette ensemble de page à pour but d'ajouter certaines fonctionalités non encore intégré dans Dolibarr 
Il ne s'agit pas d'un module dolibarr mais d'un ensemble de page php externe à'application 


## Fonctionalités :

    -Génération balance et grand livre > fonctionalité déjà présente dans Dolibarr
    -Génération de Compte de résultat et Bilan au format 2 colonnes avec configuration 
    des comptes de tiers pour le bilan 
    -Génération des écritures de cloture et d'A-nouveaux, reprennant les comptes
    auxiliaire si ils existent , 
    -Génération de rapport par compte en version détaillé (liste de compte auxiliaire, 
    balance par compte auxiliaire et grand livre par compte auxiliaire.
    -Vérification de la clotûre sur une période (permet de détécter des écritures ajouté 
    par erreur sur des exercices déjà cloturer)


## Pour fonctionnner (procédure d'installation): 

    -Vous devez placer ces pages sur un vhost de votre serveur web
    -Copier le fichier include/conf.inc.php.default en include/conf.inc.php
    -Configurer le fichier include/conf.inc.php pour donner un accés à la base de 
    données dolibarr (Seul le droit SELECT est nécessaire pour le fonctionnement)
    -Donner les droits en écriture sur le dossier 'fichier'
    -Pour utiliser les pages, vous devez disposer d'un compte utilisateur Dolibarr, 
    disposant de la permisssion de consulter les écritures du grand livre, cette permission 
    ne doit pas re hérité de son groupe (le module de connection est désactivable dans 'include/conf.inc.php'


## Notes diverses :

    -Ces pages ont été developpé sans connaissances préalables de php, avec de trés faible
    connaissances en HTML , CSS.
    -Ces pages ont été developpé pour répondre à des besoins personnels, sans aucune
    concertation/cohérences par rapport au reste du projet Dolibarr
    
    Par conséquant, vous les utilisez sous votre propre responsabilité,
    Par précaution, ne fournir que les droit SELECT sur la base dolibarr n'est peut être pas une mauvaise idée.
    
    
Pour tout retour ou question spécifique : serviceinformatique@cier14.org ou via le dépot gitlab

    
L'ensemble de pages est sous licenceGNU Affero General Public License v3.0


