<?php 
include('include/include.php');
  echo '<h1>' . $titre_du_site . '</h1>' 
        . '<h3>Du ' . $_SESSION['periode'][0]->format('j/m/Y') .' Au '  . $_SESSION['periode'][1]->format('j/m/Y') . '</h3>' ;  

//supprimer les anciens fichiers générer auparavent
purgerfichier();

//variable qui contiennent les ecritures pour generer le fichier
$ecriturefin=array() ;
$ecrituredebut=array() ;

//variable pour calculer le resultat

$resultat=0 ;

// periode sur laquelle porte la cloture 

$periode=$_SESSION['periode'] ;
$datefin=$periode[1]; // Dernier jour de l'exercice
$datedebut=date_create ( $datefin->format("Y-m-d") ) ;
$datedebut=date_modify ( $datedebut , "+1 day" ); // 1er jour de l'exercice suivant

//gestion des comptes de compte de résultat
$listecompte=lister_compte($bdd,$periode,"^[6-7]");

//pour chaque compte on le solde et on calcul le résultat
foreach($listecompte as $compte)
    {
    $parametre=parametre_compte($bdd,$periode,$compte,0) ;
    
    if ($parametre['soldedebit']>0)
        {
        $ecriturefin[]=generer_ecriture($datefin->format('Ymd') ,$datefin->format('Y-m-d') ,"Cloture du compte","CL",$compte,"",$parametre['soldedebit'],0) ;
        $resultat+=$parametre['soldedebit'] ;
        }
        
        if ($parametre['soldecredit']>0)
        {
        $ecriturefin[]=generer_ecriture($datefin->format('Ymd') ,$datefin->format('Y-m-d') ,"Cloture du compte","CL",$compte,"",0,$parametre['soldecredit']) ;
        $resultat-=$parametre['soldecredit'] ;
        }
    }

//on inscrit le resultat en a nouveaux 
if($resultat>0)
    {
    // on credite 120
    $ecriturefin[]=generer_ecriture($datefin->format('Ymd') ,$datefin->format('Y-m-d') ,"Cloture des comptes de compte de resultat","CL",891,"",0,$resultat) ;
    $ecrituredebut[]=generer_ecriture($datedebut->format('Ymd') ,$datedebut->format('Y-m-d') ,"Résultat de l'exercice précdent","AN",890,"",$resultat,0) ;
    $ecrituredebut[]=generer_ecriture($datedebut->format('Ymd') ,$datedebut->format('Y-m-d') ,"Résultat de l'exercice précdent","AN",120,"",0,$resultat) ;
    }

if($resultat<0)
    {
    // on debite 129
    $ecriturefin[]=generer_ecriture($datefin->format('Ymd') ,$datefin->format('Y-m-d') ,"Cloture des comptes de compte de resultat","CL",891,"",-$resultat,0) ;
    $ecrituredebut[]=generer_ecriture($datedebut->format('Ymd') ,$datedebut->format('Y-m-d') ,"Résultat de l'exercice précdent","AN",890,"",0,-$resultat) ;
    $ecrituredebut[]=generer_ecriture($datedebut->format('Ymd') ,$datedebut->format('Y-m-d') ,"Résultat de l'exercice précdent","AN",129,"",-$resultat,0) ;
    }

    
//gestion des comptes de bilan
$listecompte=lister_compte($bdd,$periode,"^[1-5]");

//pour chaque compte on parcour les compte aux on solde et on génère l'ecriture en AN

foreach($listecompte as $compte)
    {
    $listecompteaux=lister_compte_aux($bdd,$periode,$compte) ;

    foreach($listecompteaux as $compteaux)
        {
        
        $parametre=parametre_compte_aux($bdd,$periode,$compte,$compteaux,0) ;
    
        if ($parametre['soldedebit']>0)
            {
            $ecriturefin[]=generer_ecriture($datefin->format('Ymd') ,$datefin->format('Y-m-d') ,"Cloture du compte","CL",891,"",0,$parametre['soldedebit']) ;
            $ecriturefin[]=generer_ecriture($datefin->format('Ymd') ,$datefin->format('Y-m-d') ,"Cloture du compte","CL",$compte,$compteaux,$parametre['soldedebit'],0) ;
            $ecrituredebut[]=generer_ecriture($datedebut->format('Ymd') ,$datedebut->format('Y-m-d') ,"A Nouveaux","AN",890,"",$parametre['soldedebit'],0) ;
            $ecrituredebut[]=generer_ecriture($datedebut->format('Ymd') ,$datedebut->format('Y-m-d') ,"A Nouveaux","AN",$compte,$compteaux,0,$parametre['soldedebit']) ;
            }
        
        if ($parametre['soldecredit']>0)
            {
            $ecriturefin[]=generer_ecriture($datefin->format('Ymd') ,$datefin->format('Y-m-d') ,"Cloture du compte","CL",$compte,$compteaux,0,$parametre['soldecredit']) ;
            $ecriturefin[]=generer_ecriture($datefin->format('Ymd') ,$datefin->format('Y-m-d') ,"Cloture du compte","CL",891,"",$parametre['soldecredit'],0) ;
            $ecrituredebut[]=generer_ecriture($datedebut->format('Ymd') ,$datedebut->format('Y-m-d') ,"A Nouveaux","AN",890,"",0,$parametre['soldecredit']) ;
            $ecrituredebut[]=generer_ecriture($datedebut->format('Ymd') ,$datedebut->format('Y-m-d') ,"A Nouveaux","AN",$compte,$compteaux,$parametre['soldecredit'],0) ;
            } 
        }
    }
    
//ecriture du fichier
$path= 'fichier/cloturexercicedu' . $periode[0]->format('Y-m-d') . 'au' . $periode[1]->format('Y-m-d') . ".csv";
$fichierecriture = fopen($path,'w');

foreach($ecriturefin as $ligne)
    {
    fwrite($fichierecriture, $ligne) ;
    fwrite($fichierecriture,"\n");
    }
    
foreach($ecrituredebut as $ligne)
    {
    fwrite($fichierecriture, $ligne) ;
    fwrite($fichierecriture,"\n");
    }

fclose($fichierecriture);

echo "<p>Cette page génère automatiquement un fichier csv accesible par le lien ci dessous.<br>Ce fichier contient les écritures de cloture de l'exercice dont les date de début et de fin ont était configurer dans l'onglet configuration.<br>Ces écritures peuvent être importer via le module d'importation de dolibarr (pas de configuration particulière).<br>Les écritures : <br>-Solde les comptes de charges et de produits, calcul et affectent le résultat de l'exercice en A-Nouveaux au jour suivant de la fin de l'exercice (Compte 120 si bénéfice, 129 si déficit).<br>-Solde les comptes d'actifs et de passifs et génère l'A-Nouveaux correspondant.<br><br>Notes :<br>-Les comptes auxiliaires sont prix en compte pour générer les A-Nouveaux sur les comptes de bilan.<br>-Les comptes 890 et 891 sont utilisé pour maintenir l'équilibre du grand livres<br>-Les écritures de cloture sont enregistrées dans le journal CL, les A-nouveaux dans le journal AN<br>-Les éventuels compte de classes 8 et 9 utilisés ne sont pas traité.<br></p>" ;


echo "<br><a href='" . $path . "'>Lien vers le fichier des écriture de  cloture  généré pour la periode du " . $periode[0]->format('Y-m-d') .
                " au " . $periode[1]->format('Y-m-d') . "</a></body></html>";

?>
