<?php 
include('include/include.php');


//affichage et traitement des paramêtre du formulaires

if (isset($_POST['compte']))  // on récupère le numéro de compte si défini
    $compte=$_POST['compte'];
else $compte=0;
if(!is_numeric($compte)) // si c'est pas un nombre on le met a zéro 
    $compte=0 ;
    
if (isset($_POST['cloture']))     $cloture=true;
else   $cloture=false;
    
if (isset($_POST['grandlivre']))      $grandlivre=true;
else    $grandlivre=false;
    
if (isset($_POST['afficherlistecompteaux']))     $afficherlistecompteaux=true;
else     $afficherlistecompteaux=false;
    
if (isset($_POST['grandlivreaux']))     $grandlivreaux=true;
else    $grandlivreaux=false;

//initialisation des variables

$periode=$_SESSION['periode'] ;
$listecompte=lister_compte($bdd,$periode);
$parametre_compte=parametre_compte($bdd,$periode,$compte,$cloture);
$listecompteaux = lister_compte_aux($bdd,$periode,$compte) ;

//affichage du formulaire

echo '<center><form method="post" action="rapportcompte.php">';
echo "<p><div>Compte <select name=\"compte\">";
foreach($listecompte as $indexcompte)
    {
  echo '<option value="' . $indexcompte . '">' . $indexcompte . ' - ' . label($bdd,$indexcompte) . '</option>' ;
    }
echo '</select></div>';
echo '<div><input type="checkbox" id="cloture" name="cloture" unchecked>
        <label for="cloture">Inclure les écritures de clôture</label></div>' ;
echo '<div><input type="checkbox" id="grandlivre" name="grandlivre" unchecked>
        <label for="grandlivre">Afficher le grand livre</label></div>' ;
echo '<div><input type="checkbox" id="afficherlistecompteaux" name="afficherlistecompteaux" unchecked>
        <label for="afficherlistecompteaux">Affichage de la balance par compte auxiliaire</label></div>' ;
echo '<div><input type="checkbox" id="grandlivreaux" name="grandlivreaux" unchecked>
        <label for="grandlivreaux">Afficher un grand livre par compte auxilliaire</label></div>' ;
echo "<input type=\"submit\" value=\"Valider\" /></p></form></center><br>";

echo '<h1>' . $titre_du_site . '</h1>' 
        . '<h3>Du ' . $_SESSION['periode'][0]->format('j/m/Y') .' Au '  . $_SESSION['periode'][1]->format('j/m/Y') . '</h3>' ;  


// Affichage du tableau générale

echo '<h2>Balance</h2>';
echo "<table border=4 cellpading=50 align=center><tr align=center><th>N° Compte</th><th>Nom du Compte</th>
        <th>  Debit  </th><th>  Credit  </th><th>Solde Débiteur</th><th>Solde Créditeur</th></tr>" ;
echo    '<tr align=center ><td>' 
        . $compte 
        . '</td><td>' 
        . $parametre_compte['label']
        . "</td><td>" 
        . number_format($parametre_compte['totaldebit'], 2,',','') 
        . ' €</td><td>' 
        . number_format($parametre_compte['totalcredit'],2,',','') 
        . ' €</td><td>' ;
                
        // affichage des soldes si positif
    
        if ($parametre_compte['soldedebit'] > 0)    echo number_format($parametre_compte['soldedebit'],2,',','') . ' €' ;
        echo '</td><td>' ;
        if ($parametre_compte['soldecredit'] > 0)   echo number_format($parametre_compte['soldecredit'],2,',','') . ' €' ;
        echo '</td></tr></table><br><br>' ;



// Affichage du grand livre du Compte
if($grandlivre)
    {
    echo '<h2>Grand Livre</h2>';
    $total_debit=0 ;
    $total_credit=0 ;
    $listeecriture=recuperer_ecriture($bdd,$periode,$compte,'doc_date') ;

    // entete tableau
    echo "<table border=4 cellpading=50 align=center>";
    echo "<tr align=center><th>Date</th><th>N° Compte</th><th>Desription</th><th>Debit</th><th>Credit</th></tr>" ;

    foreach($listeecriture as $ecriture)
        {
        //affchage des donnes dans le tableaux
        echo '<tr align=center ><td>' . $ecriture['date'] . '</td><td>' . $ecriture['numerocompte'] . '</td><td>' . 
            $ecriture['label'] . '</td><td>' . $ecriture['debit'] . ' €</td><td>' . $ecriture['credit'] . ' €</td></tr>';
        $total_debit += $ecriture['debit'] ;
        $total_credit += $ecriture['credit'];
        }

    //la ligne des totaux
    echo '<tr align=center><td></td><td></td><td><b>Totaux</b></td><td>' . $total_debit . ' €</td><td>' . $total_credit . ' €</td></tr>';
    echo "</table>" ;
    }


//Affichage de la balance des compte auxiliaire

if($afficherlistecompteaux)
    {
    echo '<h2>Balance par compte auxiliaire</h2>';
    echo "<table border=4 cellpading=50 align=center><tr align=center><th>N° Compte</th><th>Nom du Compte</th>
        <th>  Debit  </th><th>  Credit  </th><th>Solde Débiteur</th><th>Solde Créditeur</th></tr>" ;
    
    foreach($listecompteaux as $compteaux)
        {
        $parametre_compte=parametre_compte_aux($bdd,$periode,$compte,$compteaux,$cloture) ;
        
        echo    '<tr align=center ><td>' 
            . $compteaux 
            . '</td><td>' 
            . $parametre_compte['label']
            . "</td><td>" 
            . number_format($parametre_compte['totaldebit'], 2,',','') 
            . ' €</td><td>' 
            . number_format($parametre_compte['totalcredit'],2,',','') 
            . ' €</td><td>' ;
               
        // affichage des soldes si positif   
        if ($parametre_compte['soldedebit'] > 0)    echo number_format($parametre_compte['soldedebit'],2,',','') . ' €' ;
        echo '</td><td>' ;
        if ($parametre_compte['soldecredit'] > 0)    echo number_format($parametre_compte['soldecredit'],2,',','') . ' €' ;
        echo '</td></tr>' ;
        }
    echo '</table><br><br>';
    }

//Affichage du grand livre par compte auxiliaire

if($grandlivreaux)
    {
    echo '<h2>Grand Livre par compte auxiliaire</h2>';
    foreach($listecompteaux as $compteaux)
        {
        if($compteaux )  echo '<h3>' . $compteaux . '</h3>' ;
        else echo '<h3>Non renseigné</h3>';
        $total_debit=0 ;
        $total_credit=0 ;
        $listeecritureaux=recuperer_ecriture_aux($bdd,$periode,$compte,$compteaux,'doc_date') ;

        // entete tableau
        echo "<table border=4 cellpading=50 align=center>";
        echo "<tr align=center><th>Date</th><th>Desription</th><th>Debit</th><th>Credit</th></tr>" ;

        foreach($listeecritureaux as $ecriture)
            {
            //affchage des donnes dans le tableaux
            echo '<tr align=center ><td>' . $ecriture['date'] . '</td><td>' . 
                $ecriture['label'] . '</td><td>' . $ecriture['debit'] . ' €</td><td>' . $ecriture['credit'] . ' €</td></tr>';
            $total_debit += $ecriture['debit'] ;
            $total_credit += $ecriture['credit'];
            }

        //la ligne des totaux
        echo '<tr align=center><td></td><td><b>Totaux</b></td><td>' . $total_debit . ' €</td><td>' . $total_credit . ' €</td></tr>';
        echo "</table>" ;
    
    
        }
    }



echo "</body></html>";
?>
 
